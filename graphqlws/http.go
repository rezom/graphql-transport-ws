package graphqlws

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"

	"gitlab.com/rezom/graphql-transport-ws/graphqlws/internal/connection"
)

// ProtocolGraphQLWS is websocket subprotocol ID for GraphQL over WebSocket
// see https://github.com/apollographql/subscriptions-transport-ws
const ProtocolGraphQLWS = "graphql-ws"

var defaultUpgrader = websocket.Upgrader{
	CheckOrigin:  func(r *http.Request) bool { return true },
	Subprotocols: []string{ProtocolGraphQLWS},
}

// NewHandlerFunc returns an http.HandlerFunc that supports GraphQL over websockets
func NewHandlerFunc(svc connection.GraphQLService, httpHandler http.Handler) http.HandlerFunc {
	return NewHandlerFuncWithUpgrader(svc, httpHandler, defaultUpgrader)
}

// NewHandlerFuncWithUpgrader returns an http.HandlerFunc that supports GraphQL over websockets
func NewHandlerFuncWithUpgrader(svc connection.GraphQLService, httpHandler http.Handler, upgrader websocket.Upgrader) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		for _, subprotocol := range websocket.Subprotocols(r) {
			if subprotocol != ProtocolGraphQLWS {
				continue
			}

			ws, err := upgrader.Upgrade(w, r, nil)
			if err != nil {
				w.Header().Set("X-WebSocket-Upgrade-Failure", err.Error())
				return
			}

			if ws.Subprotocol() != ProtocolGraphQLWS {
				w.Header().Set("X-WebSocket-Upgrade-Failure",
					fmt.Sprintf("upgraded websocket has wrong subprotocol (%s)", ws.Subprotocol()))
				ws.Close()
				return
			}

			go connection.Connect(r.Context(), ws, svc)
			return
		}

		// Fallback to HTTP
		httpHandler.ServeHTTP(w, r)
	}
}
